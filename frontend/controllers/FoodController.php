<?php

namespace frontend\controllers;

use common\models\Food;
use common\models\FoodIngredient;
use common\models\Ingredient;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class FoodController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['search'],
                'rules' => [
                    [
                        'actions' => ['search'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionSearch()
    {
        $ingIds = Yii::$app->request->get('ingredients','');

        $foods=[];
        $ingredients = Ingredient::find()
            ->where(['status' => Ingredient::STATUS_ACTIVE])
            ->asArray()
            ->all();

        $ingredients = ArrayHelper::map($ingredients,'id','name');
        if ($ingIds){
            $countMatch = count($ingIds);

            /** Если выбрано менее 2х ингредиентов  не производить поиск, выдать
            сообщение: “Выберите больше ингредиентов”. */
            if ($countMatch < 2){
                Yii::$app->session->setFlash('error','“Выберите больше ингредиентов');
                return $this->redirect(['search']);
            }

            $foods = $this->getIngredientsWithCondition($ingIds, $countMatch);

            /** Если найдены блюда с совпадением менее чем 2 ингредиента или не
            найдены вовсе вывести “Ничего не найдено”.. */
            if (count($foods) < 2){
                Yii::$app->session->setFlash('error','“Ничего не найдено');
                return $this->redirect(['search']);
            }

        }

        return $this->render('search',[
            'ingIds'=>$ingIds,
            'ingredients'=>$ingredients,
            'foods'=>$foods
        ]);
    }


    private function getIngredientsWithCondition($ingIds,$countMatch)
    {
        $filteredFoods=[];


        $foods = FoodIngredient::find()
            ->alias('fi')
            ->select(['fi.food_id','f.name','count(fi.ingredient_id) count','group_concat(DISTINCT i.name SEPARATOR \', \') ingredients '])
            ->leftJoin('foods f','f.id = fi.food_id')
            ->leftJoin('ingredients i','fi.ingredient_id = i.id')
            ->where(['f.status'=>Food::STATUS_ACTIVE])
            ->andWhere(['fi.ingredient_id'=>$ingIds])
            ->asArray()
            ->groupBy('food_id, f.name')
            ->orderBy(['count(fi.ingredient_id)'=>SORT_DESC])
            ->having('count(fi.ingredient_id) >= 2')
            ->all();

        foreach ($foods as $food){
            /** Если найдены блюда с полным совпадением ингредиентов вывеститолько их */
            if ($food['count']==$countMatch){
                $filteredFoods[] = $food;
            }
        }

        if (!empty($filteredFoods)){
            $foods = $filteredFoods;
        }

        return $foods;
    }

}
