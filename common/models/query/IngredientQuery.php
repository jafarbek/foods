<?php

namespace common\models\query;

use common\models\FoodIngredient;
use common\models\Ingredient;

class IngredientQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['status'=>Ingredient::STATUS_ACTIVE]);
    }

    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return FoodIngredient|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}