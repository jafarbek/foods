<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\FoodIngredient;
use yii\db\ActiveQuery;

/**
 * FoodIngredientSearch represents the model behind the search form of `common\models\FoodIngredient`.
 */
class FoodSearch extends Food
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 50],
            [['ingredient_ids'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Food::find()
            ->select(['f.id', 'f.name'])
            ->alias('f')
            ->where(['f.status' => Food::STATUS_ACTIVE])
            ->innerJoinWith('foodIngredients fi')
            ->innerJoinWith('foodIngredients.ingredient i');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
//            'food_id' => $this->food_id,
//            'ingredient_id' => $this->ingredient_id,
        ]);
//        debug($dataProvider->models);
        return $dataProvider;
    }
}
