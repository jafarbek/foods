<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%foods}}`.
 */
class m210915_133911_create_foods_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%foods}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string('50')->unique(),
            'status'=>$this->string('10')->defaultValue('active')

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%foods}}');
    }
}
