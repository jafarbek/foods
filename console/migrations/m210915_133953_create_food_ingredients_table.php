<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%food_ingredients}}`.
 */
class m210915_133953_create_food_ingredients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%food_ingredients}}', [
            'id' => $this->primaryKey(),
            'food_id' => $this->integer(),
            'ingredient_id' => $this->integer(),
        ]);

        $this->createIndex(
            'food_ingredients_food_id_idx',
            '{{%food_ingredients}}',
            'food_id'
        );
        $this->createIndex(
            'food_ingredients_ingredient_id_idx',
            '{{%food_ingredients}}',
            'ingredient_id'
        );

        $this->addForeignKey(
            'food_ingredients_food_id_fk',
            '{{%food_ingredients}}',
            'food_id',
            '{{%foods}}',
            'id'
        );
        $this->addForeignKey(
            'food_ingredients_ingredient_id_fk',
            '{{%food_ingredients}}',
            'ingredient_id',
            '{{%ingredients}}',
            'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('food_ingredients_food_id_fk','{{%food_ingredients}}');
        $this->dropForeignKey('food_ingredients_ingredient_id_fk','{{%food_ingredients}}');

        $this->dropIndex('food_ingredients_food_id_idx','{{%food_ingredients}}');
        $this->dropIndex('food_ingredients_ingredient_id_idx','{{%food_ingredients}}');

        $this->dropTable('{{%food_ingredients}}');
    }
}
