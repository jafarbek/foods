<?php

use common\models\Ingredient;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $food common\models\Food */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="food-ingredient-form">

    <?php $form = ActiveForm::begin([
        'id' => 'food-form',
        'enableAjaxValidation' => true,
    ]); ?>

    <?= $form->field($food, 'name')->textInput() ?>

    <?= $form->field($food, 'ingredient_ids')
        ->widget(Select2::class, [
            'data' => Ingredient::getAllActive(),
            'options' => [
                'placeholder' => Yii::t('app', 'Please, choose...'),
                'multiple'=>true,
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
