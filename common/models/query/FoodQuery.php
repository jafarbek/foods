<?php

namespace common\models\query;

use common\models\Food;
use common\models\FoodIngredient;

class FoodQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['status'=>Food::STATUS_ACTIVE]);
    }

    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return FoodIngredient|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}