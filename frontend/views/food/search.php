<?php

use common\models\Ingredient;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;


/* @var $this \yii\web\View */
/* @var $ingredients Ingredient */
/* @var $ingIds [] */
/* @var $foods [] */
?>

<div class="filter-form">
    <div id="filter-grid" class="collapse show">

        <?php ActiveForm::begin([
            'method' => 'get',
            'action' => Yii::$app->controller->action->id
        ]); ?>

        <div class='row'>
            <?php foreach ($ingredients as $id => $name): ?>
                <div class='col-md-6'>
                    <?php echo Html::checkbox("ingredients[$id]", isset($ingIds[$id]) && $ingIds[$id], ['label' => $name, 'value' => $id]); ?>
                </div>
            <?php endforeach; ?>
            <div class="col-md-12">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Cancel', [Yii::$app->controller->action->id], ['class' => 'btn btn-danger']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
<?php

if ($foods): ?>
    <div class='row'>
            <div class='col-md-6'>
                <table class="table table-bordered">
                    <thead>
                    <th>#</th>
                    <th>Name</th>
                    <th>Ingredients</th>
                    <th>Count match</th>
                    </thead>
                    <tbody>
                    <?php $i=0 ?>
                    <?php foreach ($foods as $food): ?>
                    <?php $i++; ?>
                    <tr>
                        <td><?=$i?></td>
                        <td><?=$food['name']?></td>
                        <td><?=$food['ingredients']?></td>
                        <td><?=$food['count']?></td>
                    </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
            </div>
    </div>
<?php endif?>
</div>