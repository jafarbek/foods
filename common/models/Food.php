<?php

namespace common\models;

use common\interfaces\ActiveStatusInterface;
use common\models\query\FoodQuery;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "foods".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $status
 * @property array $ingredient_ids
 *
 * @property FoodIngredient[] $foodIngredients
 */
class Food extends \common\models\base\BaseActiveRecord implements ActiveStatusInterface
{
    public $ingredient_ids;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'foods';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name','ingredient_ids'], 'required'],
            [['status'], 'string', 'max' => 10],
            [['name'], 'string', 'max' => 50],
            [['name'], 'unique','skipOnEmpty' => false, 'skipOnError' => false],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * Gets query for [[FoodIngredients]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFoodIngredients()
    {
        return $this->hasMany(FoodIngredient::className(), ['food_id' => 'id']);
    }

    public static function find()
    {
        return new FoodQuery(get_called_class());
    }

    public function ingredientsConcat(){
        $ingsString = "";
        foreach ($this->foodIngredients as $foodIngredients){
            $ingsString .= $foodIngredients->ingredient->name.', ';
        }

        $ingsString = trim($ingsString, ' ,');
        return $ingsString;
    }
}
