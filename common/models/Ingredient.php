<?php

namespace common\models;

use common\interfaces\ActiveStatusInterface;
use common\models\query\IngredientQuery;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "ingredients".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $status
 *
 * @property-read array $statuses
 * @property FoodIngredient[] $foodIngredients
 */
class Ingredient extends \common\models\base\BaseActiveRecord implements ActiveStatusInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ingredients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name','status'], 'required'],
            [['name'], 'string', 'max' => 50],
            [['status'], 'string', 'max' => 10],
            [['name'], 'unique','skipOnEmpty' => false, 'skipOnError' => false],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * Gets query for [[FoodIngredients]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFoodIngredients()
    {
        return $this->hasMany(FoodIngredient::className(), ['ingredient_id' => 'id']);
    }

    public function getStatuses(){
        return [
            self::STATUS_ACTIVE=>Yii::t('app','Active'),
            self::STATUS_INACTIVE=>Yii::t('app','Not active'),
        ];
    }

    public static function getAllActive() {
        return ArrayHelper::map(self::find()->active()->all(),'id','name');
    }

    public static function find()
    {
        return new IngredientQuery(get_called_class());
    }

    public function isActive(){
        return $this->status === self::STATUS_ACTIVE;
    }

    public function uniqueValidate($attr){
        $this->addError($attr, Yii::t('app', "must be true format"));
    }
}

