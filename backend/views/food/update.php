<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $food common\models\FoodIngredient */

$this->title = Yii::t('app', 'Update Food: {name}', [
    'name' => $food->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Food Ingredients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $food->id, 'url' => ['view', 'id' => $food->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="food-ingredient-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'food' => $food,
    ]) ?>

</div>
