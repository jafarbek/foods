<?php


namespace common\interfaces;


interface ActiveStatusInterface
{
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';
}