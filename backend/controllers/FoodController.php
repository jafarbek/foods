<?php

namespace backend\controllers;

use common\models\Food;
use common\models\FoodIngredient;
use common\models\FoodIngredientSearch;
use backend\controllers\base\BaseController;
use common\models\FoodSearch;
use Yii;
use yii\base\BaseObject;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FoodController implements the CRUD actions for FoodIngredient model.
 */
class FoodController extends BaseController
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all FoodIngredient models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FoodSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FoodIngredient model.
     * @param int $id ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FoodIngredient model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $food = new Food();

        if(Yii::$app->request->isAjax && $food->load(Yii::$app->request->post()) ) {
            Yii::$app->response->format = 'json';
            return ActiveForm::validate($food);
        }

        if ($this->request->isPost) {
            if ($food->load(Yii::$app->request->post())) {
                $this->editBlock($food);
            }
        } else {
            $food->loadDefaultValues();
        }

        return $this->render('create', [
            'food' => $food
        ]);
    }

    /**
     * Updates an existing FoodIngredient model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $food = $this->findModel($id);
        $ingredient_ids = FoodIngredient::find()->where(['food_id' => $id])->asArray()->all();
        $ingredient_ids = ArrayHelper::getColumn($ingredient_ids, 'ingredient_id');
        $food->ingredient_ids = $ingredient_ids;

        if(Yii::$app->request->isAjax && $food->load(Yii::$app->request->post()) ) {
            Yii::$app->response->format = 'json';
            return ActiveForm::validate($food);
        }


        if ($this->request->isPost && $food->load($this->request->post())) {
            $this->editBlock($food);
        }

        return $this->render('update', [
            'food' => $food,
        ]);
    }

    private function editBlock(Food &$food)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            if (!$food->isNewRecord) FoodIngredient::deleteAll(['food_id' => $food->id]);
            $food->save();
            foreach ($food->ingredient_ids as $id) {
                $foodIngredients = new FoodIngredient();
                $foodIngredients->food_id = $food->id;
                $foodIngredients->ingredient_id = $id;
                $foodIngredients->save();
            }

            $transaction->commit();
            Yii::$app->session->setFlash('success', 'Successfully saved');
            return $this->redirect(['view', 'id' => $food->id]);

        } catch (\Exception $e) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('error', 'Something went wrong');
            return $this->redirect(['index']);

        }
    }

    /**
     * Deletes an existing FoodIngredient model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FoodIngredient model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Food the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Food::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
