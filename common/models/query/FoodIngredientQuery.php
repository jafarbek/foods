<?php

namespace common\models\query;

use common\models\FoodIngredient;

/**
 * This is the ActiveQuery class for [[FoodIngredient]].
 *
 * @see FoodIngredient
 */
class FoodIngredientQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return FoodIngredient[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return FoodIngredient|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
