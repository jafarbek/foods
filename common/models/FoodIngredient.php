<?php

namespace common\models;

use common\models\query\FoodIngredientQuery;
use Yii;

/**
 * This is the model class for table "food_ingredients".
 *
 * @property int $id
 * @property int|null $food_id
 * @property int|null $ingredient_id
 *
 * @property Food $food
 * @property Ingredient $ingredient
 */
class FoodIngredient extends \common\models\base\BaseActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'food_ingredients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['food_id', 'ingredient_id'], 'required'],
            [['food_id', 'ingredient_id'], 'integer'],
            [['food_id'], 'exist', 'skipOnError' => true, 'targetClass' => Food::class, 'targetAttribute' => ['food_id' => 'id']],
            [['ingredient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ingredient::class, 'targetAttribute' => ['ingredient_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'food_id' => Yii::t('app', 'Food ID'),
            'ingredient_id' => Yii::t('app', 'Ingredient ID'),
        ];
    }


    public function getFood()
    {
        return $this->hasOne(Food::className(), ['id' => 'food_id'])->inverseOf('foodIngredients');
    }

    /**
     * Gets query for [[Ingredient]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIngredient()
    {
        return $this->hasOne(Ingredient::className(), ['id' => 'ingredient_id']);
    }

    /**
     * {@inheritdoc}
     * @return FoodIngredientQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FoodIngredientQuery(get_called_class());
    }
}
